#===============================================================================
#USAGE : ./shutdown.sh
#DESCRIPTION : Power off your machine using an script.
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
echo "want to shut down your pc now? [yes/no]"
read answer;
if [ $answer == yes ]; then 
echo "Ok, please save your files, your pc will shut down in 1 minute"
shutdown +1
elif [ $answer == no ];
then 
echo "Ok, your pc won't shutdown"
else
echo "Invalid input"
fi
#
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
