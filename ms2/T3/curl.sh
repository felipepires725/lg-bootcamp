#===============================================================================
#USAGE : ./curl.sh
#DESCRIPTION : A menu to choose between making a http request or download a file using curl. Also to check if domain is up
#PARAMS: No params 
#TIPS: Instead of params, read user input
#===============================================================================
#
#!/bin/bash
#START
echo "Menu:"
echo "(1) Make GET request and receive a JSON"
echo "(2) Download a file from a remote server"
echo "Your choice: " 
read choice;
echo "Write the domain: "
read domain;
RESPONSE="$(curl -s -I $domain)"
STATUS=$(echo $RESPONSE | grep "HTTP" | cut -d " " -f 2)

if [ $STATUS -eq "200" ];
then
	case $choice in
		1)
			echo "making request..."
			curl -x GET $domain -o get-json-response.json -L
			echo "request complete, archive saved in" 
			echo "$(cd "$(dirname "$1")" && pwd -P)/$(basename "$1")"
		;;
		2)
			echo "Downloading file..."
			curl -L $domain -O
			echo $!
	
			echo "Download complete, archive saved in" 
			echo "$(cd "$(dirname "$1")" && pwd -P)/$(basename "$1")"
	esac
else
	echo "Sorry, this domain was not find"
fi
		
	
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
